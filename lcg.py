import numpy as np # For uint32
import time # Seed generation
 
################Set these################
tmax = 100 # Maximum number that can be generated
trange = 4 # Amount of numbers to be generated
#########################################
 
class lcg(object): # Linear Congruential Generator class algo: Zi+1=(aZi+c)%m
 
# Some parts of this were taken from stackoverflow
 
    UZERO : np.uint32 = np.uint32(0)
    UONE : np.uint32 = np.uint32(1)
 
    def __init__(self, stseed: np.uint32, a: np.uint32, c: np.uint32) -> None: # Init
        self._stseed: np.uint32 = np.uint32(seed) 
        self._a   : np.uint32 = np.uint32(a)    
        self._c   : np.uint32 = np.uint32(c)    
 
    def next(self) -> np.uint32:
        self._stseed = self._a * self._stseed + self._c
        return self._stseed
 
    def stseed(self) -> np.uint32:
        return self._stseed
 
    def skseed(self, seed: np.uint32) -> np.uint32:
        self._stseed = stseed
 
    def skip(self, ns: np.int32) -> None:
 
        nskip: np.uint32 = np.uint32(ns)
 
        a: np.uint32 = self._a
        c: np.uint32 = self._c
 
        a_next: np.uint32 = lcg.UONE # Set to UONE
        c_next: np.uint32 = lcg.UZERO # Set to UZERO
 
        while nskip > lcg.UZERO:
            if (nskip & lcg.UONE) != lcg.UZERO: # Cant be 0
                a_next = a_next * a     # Skip a "a" times
                c_next = c_next * a + c # Skip c "a" times plus c
                
 
            c = (a + lcg.UONE) * c # Set c add 1
            a = a * a              # Square it 
            
 
            nskip = nskip >> lcg.UONE # Shift nskip right by UONE bits
 
        self._stseed = a_next * self._stseed + c_next # Take the step 
 
 
 
np.seterr(over='ignore') # Ignore the overflow errors lol
 
ckpdd = 2849227520832 / 1000000 # Speed of light in kilometers per day divided by 1000000
modmax = tmax + 1
 
a = np.uint32(6364136223846793005) # From Knuth rand() 
c = np.uint32(1442695040888963407) # From Knuth rand()
clk_id1 = time.CLOCK_TAI # Set clock id 
ns = time.clock_gettime_ns(clk_id1) # Get International Atomic Time (UTC+37 seconds) in nanoseconds taking leapseconds in to account
step = ns #% ckpdd
nstep = step * -1 # Inverse of step
nstep = int(nstep)
fstep = step
bstep = nstep / ckpdd # Divide nstep by ckpdd, we cant go back more than we have gone forward
bstep = int(bstep)
rseed = ns % ckpdd # Generate pseudo-random seed 
seed = np.uint32(rseed) # Set seed
rng = lcg(seed, a, c) # Call class
rng.skip(fstep) # Set forward skips
rng.skip(bstep) # Set backward skips
 
win = [] # Winner array
#debug = [] # Debug
 
# Loop
while len(win) < trange:
    rwin = rng.next() 
#   debug.append(pwin)
    pwin = rwin % modmax
    if pwin not in win:
        win.append(pwin)
                        
print("Logarithmically skipped forward", fstep,"times")
print("")
print("Logarithmically skipped backward", bstep,"times")
print("")
print("Generated with a seed of", rseed)
print("")
print(win)
#print(debug)